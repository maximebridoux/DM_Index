import numpy as np
import scipy.spatial.distance as dist
import matplotlib.pyplot as plt
from time import time
from math import sqrt
from statistics import mean,stdev

M=100
N=1000
D=4096

zero = [np.zeros(D) for i in range(N)]

def generate_vectors(N,D):
    x = np.random.randn(N,D)
    norms = dist.cdist(x, zero, 'sqeuclidean')
    for i in range(N):
        a = sqrt(norms[i][0])
        for j in range(D):
            x[i][j] = x[i][j]/a
    return x;
    
def compute_ranks(M, z, vect_pos):
    rangs = []
    for i in range(M):
        lrangs_dist = np.argsort(z[:,i])
        #lrangs[k] donne l'indice de l'élement classé en k-ème position
        #trouver le classement d'un élément indicé i revient à résoudre lrangs_dist[k] = i
        #i.e. i = np.where([lrangs_dist==k])[1][0]
        rang_dist = np.where(lrangs_dist==vect_pos)[0][0]
        rangs.append(rang_dist)
        
    return [mean(rangs), stdev(rangs)]

d = sqrt(D)
hub = [1/d for i in range(D)]
antihub = [1] + [0 for i in range(D-1)]

top = time()
x = generate_vectors(N,D) # les donnees
y = generate_vectors(M,D) # les requetes

results = np.array([])

nb_step = 200
for step in range(nb_step):
    print(step)
    vector  = [((nb_step-1) - step) / (nb_step-1) * np.array(hub) 
            +  step / (nb_step-1) * np.array(antihub)]
            
    norm = dist.cdist(vector, zero, 'sqeuclidean')[0][0]
    vector[0] = vector[0] / norm
    
    x = np.append(x, vector) #ajout du vecteur
    x = x.reshape((N+1,D))
    
    z = dist.cdist(x,y,'euclidean') # la distance euclidienne. 

    
    results = np.append(results, compute_ranks(M, z, N))
    x = np.delete(x, -1, 0) # supprime le dernier vecteur
    
results = results.reshape((nb_step,2))
axes = plt.gca()
axes.set_ylim(-100,1100)
plt.plot(results[:,0])
plt.ylabel("Rang du vecteur ajouté")
plt.xlabel("Pas du vecteur")
print("Temps de calcul (en s) : ", time() - top)
