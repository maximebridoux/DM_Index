import numpy as np
import scipy.spatial.distance as dist
import matplotlib.pyplot as plt
from time import time
from math import sqrt
from statistics import mean,stdev

def generate_vectors(N,D):
    x = np.random.randn(N,D)
    zero = [np.zeros(D) for i in range(N)]
    norms = dist.cdist(x, zero, 'sqeuclidean')
    for i in range(N):
        a = sqrt(norms[i][0])
        for j in range(D):
            x[i][j] = abs(x[i][j]/a)
    return x;
    
def compute_ranks(M, z, vect_pos):
    rangs = []
    for i in range(M):
        lrangs_dist = np.argsort(z[:,i])
        #lrangs[k] donne l'indice de l'élement classé en k-ème position
        #trouver le classement d'un élément indicé i revient à résoudre lrangs_dist[k] = i
        #i.e. i = np.where([lrangs_dist==k])[1][0]
        rang_dist = np.where(lrangs_dist==vect_pos)[0][0]
        rangs.append(rang_dist)
        
    return [mean(rangs), stdev(rangs)]

M=100
N=1000
D=8192

d = sqrt(D)
hub = [1/d for i in range(D)]
antihub = [1] + [0 for i in range(D-1)]
#antihub = list(generate_vectors(1,D))

top = time()
x = generate_vectors(N,D) # les donnees
h = np.append(hub, antihub)
x = np.append(x, h)
x = x.reshape((N+2,D))

y = generate_vectors(M,D) # les requetes
z = dist.cdist(x,y,'euclidean') # la distance euclidienne. 
# 'sqeuclidean' donnerait la dist euclidienne au carre 
    
results_hub = compute_ranks(M, z, N)
results_antihub = compute_ranks(M, z, N+1)

print("Nombre de requêtes : ", N+2)
print("Rang moyen hub : ", results_hub[0])
print("Ecart-type hub : ", results_hub[1])
print("Rang moyen antihub : ", results_antihub[0])
print("Ecart-type antihub : ", results_antihub[1])
print("Temps de calcul (en s) : ", time() - top)

