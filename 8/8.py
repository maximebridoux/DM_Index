import os
import cv2
import numpy as np
from matplotlib import pyplot as plt
from scipy.spatial import distance
from time import time
from statistics import mean


print("----------------------------------------------------------------------")
folder = '/home/maxime/Bureau/OneDrive/DM/DM_Index/Données Index/Images/Copydays/'
img1 = cv2.imread(folder + 'Copydays_original_210700_512.jpg',cv2.IMREAD_GRAYSCALE)
img2 = cv2.imread(folder + 'Copydays_strong_214301_512.jpg',cv2.IMREAD_GRAYSCALE)

def read_database(dim):
    x = []
    y = []
    for image_name in os.listdir(folder):
        image = cv2.imread(folder + image_name,cv2.IMREAD_GRAYSCALE)
        hist = cv2.calcHist(image, [0, 1, 2], None, [dim, dim, dim], [0, 256, 0, 256, 0, 256])
        cv2.normalize(hist,hist, norm_type=cv2.NORM_L2)
        if image_name.find("_original_") != -1:
            y.append(transform_hist_vect(hist, dim))
        else:
            x.append(transform_hist_vect(hist, dim))
    return [x,y]
    
def transform_hist_vect(hist, dim):
    vect = []
    for i in range(dim):
        for j in range(dim):
            for k in range(dim):
                vect.append(hist[i][j][k])
    return vect            

def compute_ratios(x,y):
    z=distance.cdist(x,y,'sqeuclidean')
    minimums=z.min(1)
    maximums=z.max(1)
    ratios=minimums/maximums
    return ratios
        
def plot_method_b(m = False):
    X = []
    Y = []
    for i in [2,4,8,10,13,16,20]: #[8, 64, 200, 256, 512, 1000, 2197, 4096, 8000]
        top = time()
        
        [x,y] = read_database(i)
        ratios = compute_ratios(x,y)

        print("Dimension :", i**3)
        print("Temps de calcul :", time()-top)

        X.append(i**3)
        if m:
            Y.append(mean(ratios))
        else:
            Y.append(ratios)
        
    p1 = plt.plot(X,Y)
    plt.xlabel("Dimension D")
    plt.ylabel("Ratios")
    if m:
        legend = "Ratios moyens des distances des 157 requêtes méthode b"
    else:
        legend = "Ratios des distances des 157 requêtes méthode b"
    plt.legend([p1[0]],[legend], loc = "best")
    plt.show()
    